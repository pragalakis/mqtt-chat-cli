const readline = require('readline');
const mqtt = require('mqtt');

// Options
const topic = 'chat';
const username = `${topic}_user_${Math.random()
  .toString(16)
  .substr(2, 8)}`;
const cyanTextColour = '\x1b[36m%s\x1b[0m';

// Init
const client = mqtt.connect(
  'mqtt://localhost:1883',
  { clientId: username }
);

// Send messages
client.on('connect', function() {
  client.subscribe(topic, function(error) {
    if (!error) {
      const clientId = client.options.clientId;
      console.log(`Welcome ${clientId}!\n`);
      const rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout,
        prompt: ``
      });

      rl.prompt();
      rl.on('line', (inputMessage) => {
        // clear the top line from the terminal
        readline.moveCursor(process.stdout, 0, -1);
        readline.clearScreenDown(process.stdout);

        // send the message
        client.publish(topic, `${clientId}> ${inputMessage}`);

        // read a new prompt
        rl.prompt();
      }).on('close', () => {
        console.log('\nBye!');
        process.exit(0);
      });
    }
  });
});

// Display incoming messages
client.on('message', function(topic, message) {
  const stringMessage = message.toString();
  const isMe = stringMessage.split('>')[0] === username;

  if (isMe) {
    console.log(cyanTextColour, stringMessage);
  } else {
    console.log(stringMessage);
  }

  //client.end();
});
