# [deprecated] A CLI Chat experiment using MQTT Pub Sub

moved to codeberg: https://codeberg.org/evasync/mqtt-chat-cli

![screenshot](screenshot.png)

## Install

`npm install`

## Start the server

`npm run server`

- It serves at: `mqtt://localhost:1883`

## Start the client

`npm run client`

- You can change the `username` if you don't want to use a random string as your username.

## Dependencies

- [MQTT.js](https://github.com/mqttjs/MQTT.js)
- [aedes](https://github.com/moscajs/aedes)
